#include "Client.h"
#include <thread>
#include <vector>
#include "hex.h"
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <md5.h>

CryptoDevice cryptoDevice;

//seif 2
/*
The function asks the user for a username and password
input:	None.
output:	The function returns 1 if the user is logged on, -1 if not. 
*/
int connect()
{
	//add users to the system.
	//before the space - the username, after the space - the password
	std::vector<std::string> accounts;
	accounts.push_back("Noa Amitay");
	accounts.push_back("Nir Barel");
	accounts.push_back("Keren Bhonker");
	accounts.push_back("Lidor Katz");

	std::string input = "";
	int found = -1, times = 3;

	std::cout << "Register secretly!" << std::endl;
	
	//the user has 3 attempts to try to connect
	//the loop runs as long as the user has not logged in yet
	while (found == -1 && times > 0)
	{
		std::getline(std::cin, input);	//get username and password
		if (std::find(accounts.begin(), accounts.end(), input) == accounts.end())	//if the user isn't in the list
		{
			std::cout << "wrong password - try again" << std::endl;
			times--;
		}
		else
		{
			size_t position = input.find(" ") + 1;	//find the space's index
			std::string name = input.substr(0, position);
			std::string message = input.substr(position);

			//md5 encrypt
			byte digest[CryptoPP::Weak1::MD5::DIGESTSIZE];

			CryptoPP::Weak1::MD5 hash;
			hash.CalculateDigest(digest, (const byte*)message.c_str(), message.length());

			CryptoPP::HexEncoder encoder;
			std::string output;

			encoder.Attach(new CryptoPP::StringSink(output));
			encoder.Put(digest, sizeof(digest));
			encoder.MessageEnd();
			//end of md5 encrypt

			//print the name and the decrypted password
			std::cout << "name: " << name << std::endl;
			std::cout << "password: " << output << std::endl;
			std::cout << "agent accepted. welcome in!" << std::endl;
			found = 1;	//to stop the loop
		}
	}

	return found;
}

int process_client(client_type &new_client)
{
	while (1)
	{
		std::memset(new_client.received_message, 0, DEFAULT_BUFLEN);

		if (new_client.socket != 0)
		{


			int iResult = recv(new_client.socket, new_client.received_message, DEFAULT_BUFLEN, 0);

            std::string strMessage(new_client.received_message);

			// some logic, we dont want to decrypt notifications sent by the operator
			// our protocol says - ": " means notification from the operator
			size_t position = strMessage.find(": ") + 2;
            std::string prefix = strMessage.substr(0, position);
            std::string postfix = strMessage.substr(position);
            std::string decrypted_message;

			//this is the only notification we use right now :(
            if (postfix != "Disconnected") {
                //please decrypt this part!
                decrypted_message = cryptoDevice.decryptAES(postfix);
            }
            else 
            {
                //dont decrypt this - not classified and has not been ecrypted! 
                //trying to do so may cause errors
                decrypted_message = postfix;
            }

            if (iResult != SOCKET_ERROR) 
            {
                std::cout << prefix + decrypted_message << std::endl;
            }            
			else
			{
                std::cout << "recv() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}
		}
	}

	if (::WSAGetLastError() == WSAECONNRESET)
        std::cout << "The server has disconnected" << std::endl;

	return 0;
}

int main()
{
	WSAData wsa_data;
	struct addrinfo *result = NULL, *ptr = NULL, hints;
    std::string sent_message = "";
	client_type client = { INVALID_SOCKET, -1, "" };
	int iResult = 0;
    std::string message;

    std::cout << "Starting Client...\n";

	// Initialize Winsock
	iResult = ::WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (iResult != 0) {
        std::cout << "WSAStartup() failed with error: " << iResult << std::endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

    std::cout << "Connecting...\n";

	// Resolve the server address and port
	iResult = getaddrinfo(IP_ADDRESS, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
        std::cout << "getaddrinfo() failed with error: " << iResult << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		client.socket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (client.socket == INVALID_SOCKET) {
            std::cout << "socket() failed with error: " << ::WSAGetLastError() << std::endl;
			::WSACleanup();
			std::system("pause");
			return 1;
		}

		// Connect to server.
		iResult = ::connect(client.socket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(client.socket);
			client.socket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (client.socket == INVALID_SOCKET) {
        std::cout << "Unable to connect to server!" << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	int isConnected = connect();
	if (isConnected == -1)
	{
		std::system("pause");
		return 1;
	}

    std::cout << "Successfully Connected" << std::endl;

	//Obtain id from server for this client;
	::recv(client.socket, client.received_message, DEFAULT_BUFLEN, 0);
	message = client.received_message;

	if (message != "Server is full")
	{
		client.id = std::atoi(client.received_message);

        std::thread my_thread(process_client, std::ref(client));

		while (1)
		{
            std::getline(std::cin, sent_message);

			//top secret! please encrypt
            std::string cipher = cryptoDevice.encryptAES(sent_message);

			iResult = ::send(client.socket, cipher.c_str(), cipher.length(), 0);

			if (iResult <= 0)
			{
                std::cout << "send() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}


		}

		//Shutdown the connection since no more data will be sent
		my_thread.detach();
	}
	else
        std::cout << client.received_message << std::endl;

    std::cout << "Shutting down socket..." << std::endl;
	iResult = ::shutdown(client.socket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
        std::cout << "shutdown() failed with error: " << ::WSAGetLastError() << std::endl;
		::closesocket(client.socket);
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	::closesocket(client.socket);
	::WSACleanup();
	std::system("pause");
	return 0;
}